--extra-index-url https://gitlab.cern.ch/api/v4/groups/33370/-/packages/pypi/simple

pyconfigdb==2.2.1 ; python_full_version >= "3.9.6" and python_full_version < "4.0.0"
    # via itk-demo-analysis-manager (pyproject.toml)