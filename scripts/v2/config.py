files = {
    "analogscan": [
        {"file": ["MeanTotMap-0.json"], "name": "as_meantotmap"}, 
        {"file": ["OccupancyMap.json"], "name": "as_occupancymap"}
    ],
    "digitalscan": [{"file": ["OccupancyMap.json"], "name": "ds_occupancymap"}],
    "thresholdscan_hr": [
        {"file": ["Chi2Map-0.json", "Chi2Map-0_0_500_0_0_0.json", "Chi2Map-0_0_300_0_0_0.json"], "name": "hr_chi2map"},
        {"file": ["NoiseMap-0.json", "NoiseMap-0_0_500_0_0_0.json", "NoiseMap-0_0_300_0_0_0.json"], "name": "hr_noisemap"},
        {"file": ["ThresholdMap-0_0_300_0_0_0.json", "ThresholdMap-0_0_500_0_0_0.json"], "name": "hr_thresholdmap"},
        {"file": ["L2_warm.json.before", "L2_cold.json.before"], "name": "hr_config"},
    ],
    "thresholdscan_hd": [
        {"file": ["Chi2Map-0.json", "Chi2Map-0_0_500_0_0_0.json", "Chi2Map-0_0_300_0_0_0.json"], "name": "hd_chi2map"},
        {"file": ["NoiseMap-0.json", "NoiseMap-0_0_500_0_0_0.json", "NoiseMap-0_0_300_0_0_0.json"], "name": "hd_noisemap"},
        {"file": ["ThresholdMap-0_0_300_0_0_0.json", "ThresholdMap-0_0_500_0_0_0.json"], "name": "hd_thresholdmap"},
        {"file": ["L2_warm.json.before", "L2_cold.json.before"], "name": "hd_config"},
    ],
    "totscan": [{"file": ["MeanTotMap-0.json"], "name": "ts_totmap"}],
    "noisescan": [
        {"file": ["NoiseOccupancy.json"], "name": "ns_noisemap"},
        {"file": ["L2_warm.json.before", "L2_cold.json.before"], "name": "ns_config"},
    ],
    "discbumpscan": [
        {"file": ["OccupancyMap.json"], "name": "db_occupancymap"},
        {"file": ["L2_warm.json.before", "L2_cold.json.before"], "name": "db_config"},
    ],
    "mergedbumpscan": [
        {"file": ["OccupancyMap.json"], "name": "mbs_occupancymap"},
        {"file": ["L2_warm.json.before", "L2_cold.json.before"], "name": "mbs_config"},
    ],
    "thresholdscan_zerobias": [
        {"file": ["Chi2Map-0.json", "Chi2Map-0_0_500_0_0_0.json", "Chi2Map-0_0_300_0_0_0.json"], "name": "tszb_chi2map"},
        {"file": ["NoiseMap-0.json", "NoiseMap-0_0_500_0_0_0.json", "NoiseMap-0_0_300_0_0_0.json"], "name": "tszb_noisemap"},
        {"file": ["ThresholdMap-0_0_300_0_0_0.json", "ThresholdMap-0_0_500_0_0_0.json"], "name": "tszb_thresholdmap"},
        {"file": ["L2_warm.json.before", "L2_cold.json.before"], "name": "tszb_config"},
    ],
    "selftrigger_source": [
        {"file": ["Occupancy.json"], "name": "sos_occupancy"},
        {"file": ["L2_warm.json.before", "L2_cold.json.before"], "name": "sos_config"},
    ],
}

modules = [
    {
        "serial": "20UPGM22110167",
        "frontends": ["0x17927", "0x17937", "0x179b6", "0x179c6"],
        "paths": [
            {
                "type": "MHT01",
                "scans": [
                    {"type": "analogscan", "path": "20UPGM22110167/MHT01/012722", "short": "as"},
                    {"type": "digitalscan", "path": "20UPGM22110167/MHT01/012721", "short": "ds"},
                    {"type": "thresholdscan_hr", "path": "20UPGM22110167/MHT01/012723", "short": "hr"},
                    {"type": "totscan", "path": "20UPGM22110167/MHT01/012724", "short": "ts"},
                ],
            },
            {
                "type": "PFA01",
                "scans": [
                    {"type": "analogscan", "path": "20UPGM22110167/PFA01/012734", "short": "as"},
                    {"type": "digitalscan", "path": "20UPGM22110167/PFA01/012733", "short": "ds"},
                    {"type": "thresholdscan_hd", "path": "20UPGM22110167/PFA01/012735", "short": "hd"},
                    {"type": "noisescan", "path": "20UPGM22110167/PFA01/012736", "short": "ns"},
                    {"type": "discbumpscan", "path": "20UPGM22110167/PFA01/012737", "short": "db"},
                ],
            }
        ],
    },
    {
        "serial": "20UPGM22110565",
        "frontends": ["0x1796b", "0x1797b", "0x1798b", "0x1799b"],
        "paths": [
            {
                "type": "MHT01",
                "scans": [
                    {"type": "analogscan", "path": "20UPGM22110565/MHT01/", "short": "as"},
                    {"type": "digitalscan", "path": "20UPGM22110565/MHT01/", "short": "ds"},
                    {"type": "thresholdscan_hr", "path": "20UPGM22110565/MHT01/", "short": "hr"},
                    {"type": "totscan", "path": "20UPGM22110565/MHT01/", "short": "ts"},
                ],
            }
        ],
    },
    {
        "serial": "20UPGM22110470",
        "frontends": ["0x1797c", "0x1798c", "0x1799c", "0x179ac"],
        "paths": [
            {
                "type": "PFA01",
                "scans": [
                    {"type": "analogscan", "path": "20UPGM22110470/PFA01/010456", "short": "as"},
                    {"type": "digitalscan", "path": "20UPGM22110470/PFA01/010455", "short": "ds"},
                    {"type": "thresholdscan_hd", "path": "20UPGM22110470/PFA01/010453", "short": "hd"},
                    {"type": "noisescan", "path": "20UPGM22110470/PFA01/010457", "short": "ns"},
                    {"type": "discbumpscan", "path": "20UPGM22110470/PFA01/010454", "short": "db"},
                ],
            }
        ],
    },
    {
        "serial": "20UPGM22110189",
        "frontends": ["0x17987", "0x17997", "0x179a7", "0x179b7"],
        "paths": [
            {
                "type": "MHT01",
                "scans": [
                    {"type": "analogscan", "path": "20UPGM22110189/cold/MHT/001177", "short": "as"},
                    {"type": "digitalscan", "path": "20UPGM22110189/cold/MHT/001176", "short": "ds"},
                    {"type": "thresholdscan_hr", "path": "20UPGM22110189/cold/MHT/001178", "short": "hr"},
                    {"type": "totscan", "path": "20UPGM22110189/cold/MHT/001179", "short": "ts"},
                ],
            },
            {
                "type": "MHT02",
                "scans": [
                    {"type": "analogscan", "path": "20UPGM22110189/warm/MHT/001053", "short": "as"},
                    {"type": "digitalscan", "path": "20UPGM22110189/warm/MHT/001052", "short": "ds"},
                    {"type": "thresholdscan_hr", "path": "20UPGM22110189/warm/MHT/001054", "short": "hr"},
                    {"type": "totscan", "path": "20UPGM22110189/warm/MHT/001055", "short": "ts"},
                ],
            },
            {
                "type": "PFA01",
                "scans": [
                    {"type": "analogscan", "path": "20UPGM22110189/cold/PFA/001188", "short": "as"},
                    {"type": "digitalscan", "path": "20UPGM22110189/cold/PFA/001187", "short": "ds"},
                    {"type": "thresholdscan_hd", "path": "20UPGM22110189/cold/PFA/001189", "short": "hd"},
                    {"type": "noisescan", "path": "20UPGM22110189/cold/PFA/001190", "short": "ns"},
                    {"type": "discbumpscan", "path": "20UPGM22110189/cold/PFA/001191", "short": "db"},
                ],
            }
        ],
    },
    {
        "serial": "20UPGM22110182",
        "frontends": ["0x17929", "0x17998", "0x179a8", "0x179d8"],
        "paths": [
            {
                "type": "MHT01",
                "scans": [
                    {"type": "analogscan", "path": "20UPGM22110182/cold/MHT/001146", "short": "as"},
                    {"type": "digitalscan", "path": "20UPGM22110182/cold/MHT/001145", "short": "ds"},
                    {"type": "thresholdscan_hr", "path": "20UPGM22110182/cold/MHT/001147", "short": "hr"},
                    {"type": "totscan", "path": "20UPGM22110182/cold/MHT/001148", "short": "ts"},
                ],
            },
            {
                "type": "PFA01",
                "scans": [
                    {"type": "analogscan", "path": "20UPGM22110182/cold/PFA/001157", "short": "as"},
                    {"type": "digitalscan", "path": "20UPGM22110182/cold/PFA/001156", "short": "ds"},
                    {"type": "thresholdscan_hd", "path": "20UPGM22110182/cold/PFA/001158", "short": "hd"},
                    {"type": "noisescan", "path": "20UPGM22110182/cold/PFA/001159", "short": "ns"},
                    {"type": "discbumpscan", "path": "20UPGM22110182/cold/PFA/001160", "short": "db"},
                    {"type": "mergedbumpscan", "path": "20UPGM22110182/cold/PFA/001161", "short": "mbs"},
                    {"type": "thresholdscan_zerobias", "path": "20UPGM22110182/cold/PFA/001163", "short": "tszb"},
                    {"type": "selftrigger_source", "path": "20UPGM22110182/cold/PFA/001165", "short": "sos"},
                ],
            }
        ],
    },
    {
        "serial": "20UPGM22200105",
        "frontends": ["0x17527", "0x175b6", "0x175c6", "0x175d6"],
        "paths": [
            {
                "type": "MHT01",
                "scans": [
                    {"type": "analogscan", "path": "20UPGM22200105/cold/MHT/001239", "short": "as"},
                    {"type": "digitalscan", "path": "20UPGM22200105/cold/MHT/001238", "short": "ds"},
                    {"type": "thresholdscan_hr", "path": "20UPGM22200105/cold/MHT/001240", "short": "hr"},
                    {"type": "totscan", "path": "20UPGM22200105/cold/MHT/001241", "short": "ts"},
                ],
            },
            {
                "type": "MHT02",
                "scans": [
                    {"type": "analogscan", "path": "20UPGM22200105/warm/MHT/001115", "short": "as"},
                    {"type": "digitalscan", "path": "20UPGM22200105/warm/MHT/001114", "short": "ds"},
                    {"type": "thresholdscan_hr", "path": "20UPGM22200105/warm/MHT/001116", "short": "hr"},
                    {"type": "totscan", "path": "20UPGM22200105/warm/MHT/001117", "short": "ts"},
                ],
            },
            {
                "type": "PFA01",
                "scans": [
                    {"type": "analogscan", "path": "20UPGM22200105/cold/PFA/001250", "short": "as"},
                    {"type": "digitalscan", "path": "20UPGM22200105/cold/PFA/001249", "short": "ds"},
                    {"type": "thresholdscan_hd", "path": "20UPGM22200105/cold/PFA/001251", "short": "hd"},
                    {"type": "noisescan", "path": "20UPGM22200105/cold/PFA/001252", "short": "ns"},
                    {"type": "discbumpscan", "path": "20UPGM22200105/cold/PFA/001253", "short": "db"},
                    {"type": "mergedbumpscan", "path": "20UPGM22200105/cold/PFA/001254", "short": "mbs"},
                    {"type": "thresholdscan_zerobias", "path": "20UPGM22200105/cold/PFA/001256", "short": "tszb"},
                    {"type": "selftrigger_source", "path": "20UPGM22200105/cold/PFA/001258", "short": "sos"},
                ],
            },
            {
                "type": "PFA02",
                "scans": [
                    {"type": "analogscan", "path": "20UPGM22200105/warm/PFA/001126", "short": "as"},
                    {"type": "digitalscan", "path": "20UPGM22200105/warm/PFA/001125", "short": "ds"},
                    {"type": "thresholdscan_hd", "path": "20UPGM22200105/warm/PFA/001127", "short": "hd"},
                    {"type": "noisescan", "path": "20UPGM22200105/warm/PFA/001128", "short": "ns"},
                    {"type": "discbumpscan", "path": "20UPGM22200105/warm/PFA/001129", "short": "db"},
                    {"type": "mergedbumpscan", "path": "20UPGM22200105/warm/PFA/001130", "short": "mbs"},
                    {"type": "thresholdscan_zerobias", "path": "20UPGM22200105/warm/PFA/001132", "short": "tszb"},
                ],
            }
        ],
    },
    {
        "serial": "20UPGM22110293",
        "frontends": ["0x1775c", "0x1776d", "0x177ac", "0x177ad"],
        "paths": [
            {
                "type": "MHT01",
                "scans": [
                    {"type": "analogscan", "path": "20UPGM22110293/cold/MHT/001208", "short": "as"},
                    {"type": "digitalscan", "path": "20UPGM22110293/cold/MHT/001207", "short": "ds"},
                    {"type": "thresholdscan_hr", "path": "20UPGM22110293/cold/MHT/001209", "short": "hr"},
                    {"type": "totscan", "path": "20UPGM22110293/cold/MHT/001210", "short": "ts"},
                ],
            },
            {
                "type": "MHT02",
                "scans": [
                    {"type": "analogscan", "path": "20UPGM22110293/warm/MHT/001084", "short": "as"},
                    {"type": "digitalscan", "path": "20UPGM22110293/warm/MHT/001083", "short": "ds"},
                    {"type": "thresholdscan_hr", "path": "20UPGM22110293/warm/MHT/001085", "short": "hr"},
                    {"type": "totscan", "path": "20UPGM22110293/warm/MHT/001086", "short": "ts"},
                ],
            },
            {
                "type": "PFA01",
                "scans": [
                    {"type": "analogscan", "path": "20UPGM22110293/cold/PFA/001219", "short": "as"},
                    {"type": "digitalscan", "path": "20UPGM22110293/cold/PFA/001218", "short": "ds"},
                    {"type": "thresholdscan_hd", "path": "20UPGM22110293/cold/PFA/001220", "short": "hd"},
                    {"type": "noisescan", "path": "20UPGM22110293/cold/PFA/001221", "short": "ns"},
                    {"type": "discbumpscan", "path": "20UPGM22110293/cold/PFA/001222", "short": "db"},
                    {"type": "mergedbumpscan", "path": "20UPGM22110293/cold/PFA/001223", "short": "mbs"},
                    {"type": "thresholdscan_zerobias", "path": "20UPGM22110293/cold/PFA/001225", "short": "tszb"},
                    {"type": "selftrigger_source", "path": "20UPGM22110293/cold/PFA/001227", "short": "sos"},
                ],
            },
            {
                "type": "PFA02",
                "scans": [
                    {"type": "analogscan", "path": "20UPGM22110293/warm/PFA/001095", "short": "as"},
                    {"type": "digitalscan", "path": "20UPGM22110293/warm/PFA/001094", "short": "ds"},
                    {"type": "thresholdscan_hd", "path": "20UPGM22110293/warm/PFA/001096", "short": "hd"},
                    {"type": "noisescan", "path": "20UPGM22110293/warm/PFA/001097", "short": "ns"},
                    {"type": "discbumpscan", "path": "20UPGM22110293/warm/PFA/001098", "short": "db"},
                    {"type": "mergedbumpscan", "path": "20UPGM22110293/warm/PFA/001099", "short": "mbs"},
                    {"type": "thresholdscan_zerobias", "path": "20UPGM22110293/warm/PFA/001101", "short": "tszb"},
                    {"type": "selftrigger_source", "path": "20UPGM22110293/warm/PFA/001103", "short": "sos"},
                ],
            }
        ],
    },   
    {
        "serial": "20UPGM22110471",
        "frontends": ["0x1794d", "0x1795d", "0x1796d", "0x1797d"],
        "paths": [
            {
                "type": "PFA01",
                "scans": [
                    {"type": "analogscan", "path": "20UPGM22110471/PFA01/010464", "short": "as"},
                    {"type": "digitalscan", "path": "20UPGM22110471/PFA01/010463", "short": "ds"},
                    {"type": "thresholdscan_hd", "path": "20UPGM22110471/PFA01/010461", "short": "hd"},
                    {"type": "noisescan", "path": "20UPGM22110471/PFA01/010465", "short": "ns"},
                    {"type": "discbumpscan", "path": "20UPGM22110471/PFA01/010462", "short": "db"},
                ],
            },
        ],
    },
    {
        "serial": "20UPGR92110514",
        "frontends": ["0x1475a", "0x1476a", "0x1477a", "0x1478a"],
        "paths": [
            {
                "type": "MHT01",
                "scans": [
                    {"type": "analogscan", "path": "20UPGR92110514/MHT01/000214", "short": "as"},
                    {"type": "digitalscan", "path": "20UPGR92110514/MHT01/000213", "short": "ds"},
                    {"type": "thresholdscan_hr", "path": "20UPGR92110514/MHT01/000215", "short": "hr"},
                ],
            },
            {
                "type": "MHT02",
                "scans": [
                    {"type": "analogscan", "path": "20UPGR92110514/MHT02/000210", "short": "as"},
                    {"type": "digitalscan", "path": "20UPGR92110514/MHT02/000209", "short": "ds"},
                    {"type": "thresholdscan_hr", "path": "20UPGR92110514/MHT02/000211", "short": "hr"},
                ],
            },
            {
                "type": "MHT03",
                "scans": [
                    {"type": "analogscan", "path": "20UPGR92110514/MHT03/000205", "short": "as"},
                    {"type": "digitalscan", "path": "20UPGR92110514/MHT03/000203", "short": "ds"},
                    {"type": "thresholdscan_hr", "path": "20UPGR92110514/MHT03/000207", "short": "hr"},
                ],
            },
            {
                "type": "MHT04",
                "scans": [
                    {"type": "analogscan", "path": "20UPGR92110514/MHT04/000200", "short": "as"},
                    {"type": "digitalscan", "path": "20UPGR92110514/MHT04/000199", "short": "ds"},
                    {"type": "thresholdscan_hr", "path": "20UPGR92110514/MHT04/000201", "short": "hr"},
                ],
            },
        ],
    },
]

lls_list = [
    {
        "author": "analysis_rk_script",
        "comment": "Outer Barrel Longeron",
        "metadata": {"lls_type": "Longeron", "nMods": 36},
        "name": "LLS_OB_0001",
        "type": "lls",
    },
    {
        "author": "analysis_rk_script",
        "comment": "Outer Barrel Longeron",
        "metadata": {"lls_type": "Longeron", "nMods": 36},
        "name": "LLS_OB_0002",
        "type": "lls",
    },
    # {
    #     "author": "analysis_rk_script",
    #     "comment": "Outer Barrel Half Ring",
    #     "metadata": {"lls_type": "Half-Ring-OB", "nMods": 28},
    #     "name": "LLS_OB_0003",
    #     "type": "lls",
    # },
    # {
    #     "author": "analysis_rk_script",
    #     "comment": "Outer Barrel Longeron",
    #     "metadata": {"lls_type": "Longeron", "nMods": 36},
    #     "name": "LLS_OB_0004",
    #     "type": "lls",
    # },
    # {
    #     "author": "analysis_rk_script",
    #     "comment": "Outer Barrel Longeron",
    #     "metadata": {"lls_type": "Longeron", "nMods": 36},
    #     "name": "LLS_OB_0005",
    #     "type": "lls",
    # },
    # {
    #     "author": "analysis_rk_script",
    #     "comment": "Outer Barrel Half Ring",
    #     "metadata": {"lls_type": "Half-Ring-OB", "nMods": 28},
    #     "name": "LLS_OB_0006",
    #     "type": "lls",
    # },
    {
        "author": "analysis_rk_script",
        "comment": "End Cap Half Ring",
        "metadata": {"lls_type": "Half-Ring-EC", "nMods": 26},
        "name": "LLS_EC_0001",
        "type": "lls",
    },
    {
        "author": "analysis_rk_script",
        "comment": "End Cap Half Ring",
        "metadata": {"lls_type": "Half-Ring-EC", "nMods": 26},
        "name": "LLS_EC_0002",
        "type": "lls",
    },
]
