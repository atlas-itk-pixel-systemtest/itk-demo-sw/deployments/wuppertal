from pyconfigdb import ConfigDB
from lls_pdb.config import configdb_key, sr_url, lls_name, num_processes, files, modules
from lls_pdb.helper import traverse_tree
from multiprocessing import Pool
from uuid import uuid4
from collections import defaultdict
import logging
import json
import os

logging.basicConfig(level=logging.INFO, format="%(asctime)s - %(message)s", datefmt="%H:%M:%S")
log = logging.getLogger(__name__)


def get_serials():
    configdb = ConfigDB(dbKey=configdb_key, srUrl=sr_url)
    tree = configdb.tag_tree(name=lls_name, stage=False, payload_data=False)
    serials = traverse_tree(tree["objects"][0])
    return serials


def create_tags(scan_tags: dict):
    configdb = ConfigDB(dbKey=configdb_key, srUrl=sr_url)

    scan_tag_names = []
    for key, value in scan_tags.items():
        scan_tag_name = f"{lls_name}_{key}"
        configdb.tag_create(name=scan_tag_name, type="scan-tag", payloads=value, stage=False)
        scan_tag_names.append(scan_tag_name)
    configdb.tag_create(name=f"{lls_name}_scan-tags", type="scan-tags", members=scan_tag_names, stage=False)


def create_scan_tags(serials: dict):
    with Pool(processes=num_processes) as pool:
        results = pool.starmap(add_to_module, [(fe_serials, i, len(serials)) for i, fe_serials in enumerate(serials.values())])

    scan_tags = defaultdict(list)
    for d in results:
        for scan_tag, payloads in d[1].items():
            scan_tags[scan_tag].extend(payloads)

    create_tags(scan_tags=scan_tags)


def add_to_module(serials, i, amount):
    log.info(f"Adding scans to module {i + 1}/{amount}")
    configdb = ConfigDB(dbKey=configdb_key, srUrl=sr_url)
    payloads = []

    module_data = modules[i % len(modules)]

    scan_tags = defaultdict(list)
    module_payloads = []
    module_id = serials["id"]
    first_fe = True
    for j, frontend in enumerate(serials["fes"].values()):
        frontend_id = frontend["id"]
        payloads = []

        for scan_tag in module_data["paths"]:
            payload_ids = []
            paths = {}

            for scan in scan_tag["scans"]:
                std = "_std_"
                if scan["type"] == "selftrigger_source":
                    std = "_"
                path = f"/data/input/{scan['path']}{std}{scan['type']}"
                paths[f"{scan['short']}_path"] = path

                for file in files[scan["type"]]:
                    found = False
                    for filename in file["file"]:
                        file_name = f"{module_data['frontends'][j]}_{filename}"
                        file_path = os.path.join(path, file_name)
                        try:
                            with open(file_path, "r") as f:
                                data = json.load(f)
                        except FileNotFoundError:
                            continue
                        payl_id = uuid4().hex
                        payloads.append({"type": file["name"], "data": json.dumps(data), "name": file_name, "id": payl_id})
                        payload_ids.append(payl_id)
                        found = True
                    if not found:
                        log.error(f"File not found: {file_path}")
                        # os._exit(-1)

                if first_fe:
                    file_path = os.path.join(path, "scanLog.json")
                    try:
                        with open(file_path, "r") as f:
                            data = json.load(f)
                    except FileNotFoundError:
                        log.error(f"File not found: {file_path}")
                    module_payload_id = uuid4().hex
                    module_payloads.append({"type": f"{scan['short']}_scanlog", "data": json.dumps(data), "name": "scanLog.json", "id": module_payload_id})
                    payload_ids.append(module_payload_id)

            if first_fe:
                module_path_payload_id = uuid4().hex
                module_payloads.append({"type": "paths", "data": json.dumps(paths), "id": module_path_payload_id})
                payload_ids.append(module_path_payload_id)

            scan_tags[scan_tag["type"]].extend(payload_ids)

        first_fe = False

        configdb.add_to_node(id=frontend_id, payloads=payloads, stage=False)
        log.info(f"Added scans to fe {j + 1}/4 for module {i + 1}/{amount}.")

    configdb.add_to_node(id=module_id, payloads=module_payloads, stage=False)
    log.info(f"Added scans to module: {i + 1}/{amount}.")
    return module_id, scan_tags
