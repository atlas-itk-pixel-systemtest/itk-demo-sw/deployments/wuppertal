from lls_pdb.config import log
from lls_pdb.helper import standard_post
from lls_pdb.models import InputTree
from uuid import uuid4

serial_count = 1001
counter = 1

def create_layer(data: InputTree):
    global counter
    serials = []
    
    for j in range(0, data.amount):
        child_serials = []
        # create children layer
        if data.children:
            for child in data.children:
                child_serials.extend(create_layer(child,))

        # create this layer
        serial = create_component(data, child_serials)
        serials.append(serial)
        counter += 1

    return serials


def create_component(data: InputTree, child_serials):
    global counter
    global serial_count

    if "PART_NUMBER" in data.properties:
        data.properties["PART_NUMBER"] = f"BUW_{uuid4().hex}"

    if data.serialNumber:
        serialNumber = f"{data.serialNumber}{str(serial_count).zfill(5)}"
        serial_count += 1
    else:
        serialNumber = None

    comp = {
        "project": "P",
        "subproject": data.subproject,
        "institution": "BUW",
        "componentType": data.componentType,
        "type": data.type,
        "properties": data.properties,
        "assemble_childs": child_serials,
        "serialNumber": serialNumber
    }
    success, response = standard_post(api_endpoint="/component/", body=comp)
    if success:
        serial = response["component"]["serialNumber"]
        log.info(f"{counter}: {data.componentType}, {serial}: creation successful")
        return serial
    else:
        log.error(f"{counter}: {data.componentType}: creation failed")
        log.error(response)
        exit(1)


def assemble(parent, child):
    body = {parent: parent, child: child}
    success, response = standard_post(api_endpoint="/", body=body, endpoint="assembleComponent", httpMethod="post")
    return success
