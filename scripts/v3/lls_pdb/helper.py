from lls_pdb.config import url, log, headers
from requests import post, get
import json

def traverse_tree(lls_data):
    serials = {}
    for child in lls_data["children"]:
        if child["type"] == "MODULE":
            fe_mapping = traverse_tree(child)
            serials[child["metadata"]["serial"]] = {"id": child["id"], "fes": fe_mapping}
        elif child["type"] == "FE_CHIP":
            serials[child["metadata"]["serial"]] = {"id": child["id"]}
        else:
            serials.update(traverse_tree(child))
    return serials

def standard_post(api_endpoint, body={}, **parameter):
    try:
        response = post(f"{url}{api_endpoint}", params=parameter, data=json.dumps(body), headers=headers)
    except ConnectionError:
        log.error(f"error connection to {url}")
        return False, json.loads(response.content)
    if not response.status_code // 100 == 2:
        # log.error("HTTP response error")
        return False, json.loads(response.content)
    return True, json.loads(response.content)


def standard_get(api_endpoint, **parameter):
    try:
        response = get(f"{url}{api_endpoint}", params=parameter, headers=headers)
    except ConnectionError:
        log.error(f"error connection to {url}")
        return False, json.loads(response.content)
    if not response.status_code // 100 == 2:
        # log.error("HTTP response error")
        return False, json.loads(response.content)
    return True, json.loads(response.content)
