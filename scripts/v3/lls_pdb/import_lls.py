from lls_pdb.helper import standard_post, traverse_tree
from lls_pdb.config import lls_name, configdb_key, sr_url
from pyconfigdb import ConfigDB
import json

config = {
    "OB_LOADED_LONGERON": {"attachments": []},
    "PP0_LONGERON": {"attachments": []},
    "OB_LOADED_INCLINED_HALF-RING": {"attachments": []},
    "OB_INCLINED_PP0": {"attachments": []},
    "OB_VIRTUAL_HV_GROUP": {"attachments": []},
    "OB_LOADED_MODULE_CELL": {"attachments": []},
    "MODULE": {"attachments": []},
    "BARE_MODULE": {"attachments": []},
    "FE_CHIP": {"attachments": [{"type": "config", "title": "L2_warm"}]},
    "SENSOR_TILE": {"attachments": []},
}


def import_lls(serial):
    success, response = standard_post(api_endpoint="/component/tree", body=config, componentSerial=serial, strict=True, metadata=True, configdb=lls_name)
    return success


def commit_lls():
    configdb = ConfigDB(dbKey=configdb_key, srUrl=sr_url)
    configdb.stage_commit(identifier=lls_name, new_name=lls_name, type="lls", comment="Dummy LLS", author="Jonas Schmeing")


def add_metadata():
    configdb = ConfigDB(dbKey=configdb_key, srUrl=sr_url)
    tree = configdb.tag_tree(name=lls_name, stage=True, payload_data=False)
    serials = traverse_tree(tree["objects"][0])

    configdb.payload_create(type="runkey_meta", data=json.dumps({"lls_type": "DUMMY-LLS", "nMods": len(serials)}), meta=True, tags=[lls_name])

    for i, module in enumerate(serials.values()):
        update_metadata(module["id"], {"position": i + 1})
        for j, frontend in enumerate(module["fes"].values()):
            update_metadata(frontend["id"], {"position": j + 1})

    return serials


def update_metadata(uuid, new_values):
    configdb = ConfigDB(dbKey=configdb_key, srUrl=sr_url)

    obj = configdb.object_read(uuid)
    payload_id = obj["metadata_ids"][0]
    metadata = obj["metadata"]
    metadata.update(new_values)
    configdb.payload_update(id=payload_id, meta=True, data=metadata)
