from pydantic import BaseModel
from typing import ForwardRef, Optional


InputTree = ForwardRef("InputTree")


class InputTree(BaseModel):
    componentType: str
    type: str
    amount: int
    properties: dict = {}
    subproject: str
    serialNumber: Optional[str] = None
    children: list[InputTree] = []
