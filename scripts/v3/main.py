from lls_pdb.create_lls import create_layer
from lls_pdb.add_scans import create_scan_tags, get_serials
from lls_pdb.import_lls import import_lls, add_metadata, commit_lls
from lls_pdb.models import InputTree
import json


def main_create():
    with open("scripts/v3/lls_definition.json", "r") as f:
        data = json.load(f)
    data = InputTree.model_validate(data)
    serials = create_layer(data)
    return serials[0]


def main_import(serial):
    import_lls(serial)
    add_metadata()
    commit_lls()


def main_fill():
    serials = get_serials()
    create_scan_tags(serials=serials)


if __name__ == "__main__":
    # serial = main_create()
    serial = "20UPBZU0000004"
    main_import(serial)
    main_fill()
